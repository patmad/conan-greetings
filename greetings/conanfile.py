from conan import ConanFile
from conan.tools.cmake import CMake, cmake_layout

class GreetingsRecipe(ConanFile):
    name = "greetings"
    version = "1.0"

    settings = "os", "compiler", "build_type", "arch"

    options = {
        "shared": [True, False],
        "fPIC": [True, False]
    }

    default_options = {
        "shared": False,
        "fPIC": True
    }

    exports_sources = "src/*", "include/*", "CMakeLists.txt"

    generators = "CMakeDeps", "CMakeToolchain"

    requires = "hello/1.0", "bye/1.0"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def configure(self):
        if self.options.shared:
            del self.options.fPIC

    def layout(self):
        cmake_layout(self)

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["greetings"]

        self.cpp_info.set_property("cmake_file_name", "MYG")
        self.cpp_info.set_property("cmake_target_name", "MyGreetings::MyGreetings")
