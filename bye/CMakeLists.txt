cmake_minimum_required(VERSION 3.1)
project(bye CXX)

add_library(bye src/bye.cpp)
target_include_directories(bye PUBLIC include)

install(TARGETS bye
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib
    RUNTIME DESTINATION bin
)

install(DIRECTORY include/
    DESTINATION include
    FILES_MATCHING PATTERN "*.h"
)
