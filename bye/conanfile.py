from conan import ConanFile
from conan.tools.cmake import CMake, cmake_layout

class ByeRecipe(ConanFile):
    name = "bye"
    version = "1.0"

    settings = "os", "compiler", "build_type", "arch"

    options = {
        "shared": [True, False],
        "fPIC": [True, False]
    }

    default_options = {
        "shared": False,
        "fPIC": True
    }

    exports_sources = "src/*", "include/*", "CMakeLists.txt"

    generators = "CMakeDeps", "CMakeToolchain"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def configure(self):
        if self.options.shared:
            del self.options.fPIC

    def layout(self):
        cmake_layout(self)

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["bye"]

        self.cpp_info.set_property("cmake_file_name", "MYB")
        self.cpp_info.set_property("cmake_target_name", "MyBye::MyBye")
